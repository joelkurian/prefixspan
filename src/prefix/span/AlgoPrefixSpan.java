package prefix.span;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JOptionPane;


 /*  This program generates sequential patterns with constraints 
 */
public class AlgoPrefixSpan extends AbstractAlgoPrefixSpan{
	// The sequential patterns that are found
	public Sequences patterns = null;
	
	// for statistics
	public long startTime;
	
	// parameters of runAlgorithm
	private final double minsupp;
	private final double minInterval;
	private final double maxInterval;
	private final double minWholeInterval;
	private final double maxWholeInterval;
	private int minsuppRelative;
	public  long projectedCount;
	private JFrame frame=new JFrame();
	private PseudoSequenceDatabase initialContext = null;

	
	
	public AlgoPrefixSpan(double minsupp,double minInterval, double maxInterval,double minWholeInterval, double maxWholeInterval){
		if((minInterval > maxInterval) ||
				(minWholeInterval > maxWholeInterval) ||
				(minInterval > maxWholeInterval) ||
				(maxInterval > maxWholeInterval)){
			JOptionPane.showMessageDialog(frame, "Parameters are not valid!!!","Error", JOptionPane.ERROR_MESSAGE);

				throw new RuntimeException("Parameters are not valid!!!");
			}	
		this.minsupp = minsupp;
		this.minInterval = minInterval;
		this.maxInterval = maxInterval;
		this.minWholeInterval = minWholeInterval;
		this.maxWholeInterval = maxWholeInterval;	
	}
	
	public Sequences runAlgorithm(SequenceDatabase contexte) {
		patterns = new Sequences("FREQUENT SEQUENCES WITH TIMESTAMP");
		this.minsuppRelative = (int) Math.ceil(minsupp * contexte.size());
		if(this.minsuppRelative == 0){ // protection
			this.minsuppRelative = 1;
		}
		startTime = System.currentTimeMillis();
	//	contexte.print();
		prefixSpan(contexte);
		return patterns;
	}

	
	/**
	 * This method generates sequential patterns
	 */
	private void prefixSpan(SequenceDatabase contexteInitial){
		// Scan the database to find all frequent patterns of size 1.
		// and maps the sequences in which these patterns appear.
		Map<Item, Set<Integer>> mapSequenceID = findSequencesContainingItems(contexteInitial);
		// System.out.println(mapSequenceID.toString());
		// convert the database in pseudo-database ,and remove
		// the items of size 1 that are not frequent , so that the algorithm  
		// will not consider them anymore 
		initialContext = new PseudoSequenceDatabase();
		//System.out.println(minsuppRelative);
		for(Sequence sequence : contexteInitial.getSequences()){
			Sequence optimizedSequence = sequence.cloneSequenceMinusItems(mapSequenceID, minsuppRelative);
			if(optimizedSequence.size() != 0){
				initialContext.addSequence(new PseudoSequence(0, optimizedSequence, 0, 0));
			}
			
		}
		//initialContext.printContext();
		// For each item
		for(Entry<Item, Set<Integer>> entry : mapSequenceID.entrySet()){
			if(entry.getValue().size() >= minsuppRelative){ // if item is frequent
				Item item = entry.getKey();
			//	System.out.println(entry.getKey()); //prints the frequent item 
				PseudoSequenceDatabase[] projectedContexts = null;
					projectedContexts = buildProjectedContext(item, initialContext,  false, -1);
				// For each projected database
				for(PseudoSequenceDatabase projectedContext : projectedContexts){
					//if(item.getId()==1)
					//projectedContext.printContext();
					    Sequence prefix = new Sequence(0);  
						prefix.addItemset(new Itemset(item, 1));
						prefix.setSequencesID(entry.getValue());
					//	prefix.print();
				//	}
			 		projection(prefix, 2, projectedContext);
							patterns.addSequence(prefix, 1); 
				}
			}
		}		
	}


	/**
	 * Find all sequences that contains an item.
	 * Maps items and Set of sequences that contains each of them.
	 */
	private Map<Item, Set<Integer>> findSequencesContainingItems(SequenceDatabase contexte) {
		Set<Integer> alreadyCounted = new HashSet<Integer>(); 
		Sequence lastSequence = null;
		Map<Item, Set<Integer>> mapSequenceID = new HashMap<Item, Set<Integer>>(); 
		for(Sequence sequence : contexte.getSequences()){
			if(lastSequence == null || lastSequence.getId() != sequence.getId()){ 
				alreadyCounted.clear(); 
				lastSequence = sequence;
			}
			for(Itemset itemset : sequence.getItemsets()){
				for(Item item : itemset.getItems()){
					if(!alreadyCounted.contains(item.getId())){
						Set<Integer> sequenceIDs = mapSequenceID.get(item);
						if(sequenceIDs == null){
							sequenceIDs = new HashSet<Integer>();
							mapSequenceID.put(item, sequenceIDs);
						}
						sequenceIDs.add(sequence.getId());
						alreadyCounted.add(item.getId()); 
					}
				}
			}
		}
		return mapSequenceID;
	}

	/**
	 * This method scans projected database to find all pairs of item.
	 * parameters are : prefixe, k  Level, initial contexte
	 */
	private void projection(Sequence prefixe, int k, PseudoSequenceDatabase contexte) {	
	
	/*Method to find all frequent items in a context (database).
	 * This is for k> 1.
	 */
		for(Pair paire : findAllFrequentPairsSatisfyingC1andC2(prefixe, contexte.getPseudoSequences())){ //checks for gap constraint
			// check item in postfix is frequent.
			if(paire.getCount() >= minsuppRelative){
				Sequence newPrefix;
				if(paire.isPostfix()){
					newPrefix = appendItemToPrefixOfSequence(prefixe, paire.getItem());
				}else{
					newPrefix = appendItemToSequence(prefixe, paire.getItem(), paire.getTimestamp());
				}
			//	if(isMaxWholeIntervalRespected(newPrefix)){
				projectionPair(newPrefix, paire, prefixe, contexte, k);
			//	}
			}
		}
	}
	
	/**
	 *
	 * This method recursively generates projected database
	 * parameters are:newPrfix,paire - new item, prefixe- Original prefixe, contexte, k Level
	 */
	private void projectionPair(Sequence newPrefix, Pair paire, Sequence prefixe, PseudoSequenceDatabase contexte, int k) {
	
		PseudoSequenceDatabase[] projectedContexts = null;
			projectedContexts = buildProjectedContext(paire.getItem(), contexte, paire.isPostfix(), paire.getTimestamp());
						
		for(PseudoSequenceDatabase projectedContext : projectedContexts){
			Sequence prefix;
			if(projectedContext.getSequence() == null){ 
				prefix = newPrefix.cloneSequence();
				prefix.setSequencesID(paire.getSequencesID());
				
				//prefix.print();
			}
			else{ 
				Set<Integer> sequenceIDs = projectedContext.getSequenceIDs();
				if(paire.isPostfix()){
					prefix = appendItemToPrefixOfSequence(prefixe, paire.getItem()); 
				}else{
					prefix = appendItemToSequence(prefixe, paire.getItem(), paire.getTimestamp());  
				}
				prefix.setSequencesID(sequenceIDs); 
			}
			projection(prefix, k+1, projectedContext); // r�cursion
			if(compactnessConstraint(prefix)){
					patterns.addSequence(prefix, prefix.size());  // merge local frequent patterns
			}	
		}
	}

	/**
	 * Method to find all frequent items in a context (database) for k>1.
	 */
	protected Set<Pair> findAllFrequentPairsSatisfyingC1andC2(Sequence prefixe, List<PseudoSequence> sequences) {
      //	scan the database and store the cumulative support of each pair in a map.
 		Map<Pair, Pair> mapPaires = new HashMap<Pair, Pair>();
		PseudoSequence lastSequence = null;
		Set<Pair> alreadyCountedForSequenceID = new HashSet<Pair>();// to count each item,  only one time for each sequence ID

		for(PseudoSequence sequence : sequences){
	//		prefixe.print();
			//sequence.print();
			if(lastSequence == null || sequence.getId() != lastSequence.getId()){  
				alreadyCountedForSequenceID.clear(); 
				lastSequence = sequence;
			}
		//	sequence.print();
		//	System.out.println(" size:"+sequence.size());
			for(int i=0; i< sequence.size(); i++){
				for(int j=0; j < sequence.getSizeOfItemsetAt(i); j++){
					Item item = sequence.getItemAtInItemsetAt(j, i);
					if(gapConstraint(sequence.getTimeStamp(i)) ||(sequence.isPostfix(i))){ // C1 C2 check
						Pair paire = new Pair(sequence.getTimeStamp(i), sequence.isPostfix(i), item);
						Pair oldPaire = mapPaires.get(paire);
						if(!alreadyCountedForSequenceID.contains(paire)){
							if(oldPaire == null){
								mapPaires.put(paire, paire); //put(paire key, paire value);
							}else{
								paire = oldPaire;
							}
							alreadyCountedForSequenceID.add(paire);
							paire.getSequencesID().add(sequence.getId());
						}
					}
				}
			}
		}
		return mapPaires.keySet();
	}
	
//	 C1 & C2 (gap constraint)
	public boolean gapConstraint(long itemsetTimestamp){
		return (itemsetTimestamp >= minInterval) && (itemsetTimestamp <= maxInterval);
		
	}
//	 C3 & C4 (compactness constraint)
	public boolean compactnessConstraint(Sequence sequence){  
		return  (sequence.get(sequence.size()-1).getTimestamp() >= minWholeInterval) && (sequence.get(sequence.size()-1).getTimestamp()<= maxWholeInterval);
	}
	
	/**
	 * This method builds projected database
	 * parameters are :item The item to use to make the projection
	 * context -The current database.
	 * inSuffix -This boolean indicates if the item "item" is part of a suffix or not.
	 * timestamp -timestamp of current item
	  */
	private PseudoSequenceDatabase[] buildProjectedContext(Item item, 
			PseudoSequenceDatabase contexte, boolean inSuffix, long timestamp) {
		
	//	contexte.printContext();	
		projectedCount++;
		PseudoSequenceDatabase sequenceDatabase = new PseudoSequenceDatabase();

		
		for(PseudoSequence sequence : contexte.getPseudoSequences()){
			for(int i =0; i< sequence.size(); i++){ 
				// System.out.println(sequence.size());  //no. of itemsets in sequence
				if(timestamp != -1 && timestamp != sequence.getTimeStamp(i)){
					continue;
				}
				
				int index = sequence.indexOf(i, item.getId()); //returns the position of item in the current itemset
				   // if(item.getId()==1 && sequence.getId()==2)
				   // 	System.out.println(sequence.isPostfix(i));
				if(index != -1 && sequence.isPostfix(i) == inSuffix){  
					if(index != sequence.getSizeOfItemsetAt(i)-1){  //if it is not the last item of itemset
						PseudoSequence newSequence = new PseudoSequence(sequence.getAbsoluteTimeStamp(i), 
								sequence, i, index+1);
					//	if(item.getId()==2){
					//	  newSequence.print();
					//	}
					//	int p=newSequence.size();
						if(newSequence.size() >0){ 
							sequenceDatabase.addSequence(newSequence);
						} 
					}else if (i != sequence.size()-1){  // if item is the last item of itemset and if itemset is not the last itemset of seq.			 
						PseudoSequence newSequence = new PseudoSequence(sequence.getAbsoluteTimeStamp(i), sequence, i+1, 0);
						if(newSequence.size() >0){
							sequenceDatabase.addSequence(newSequence);
						}	
					}	
				}
			}
		}

		return new PseudoSequenceDatabase[]{sequenceDatabase};
	}
	
    /**
    * This method creates a copy of the sequence and add the item to the sequence. It sets the 
	*  support of the sequence as the support of the item.
	*/
	private Sequence appendItemToSequence(Sequence prefix, Item item, long timestamp) {
		Sequence newPrefix = prefix.cloneSequence();  // isSuffix
		long pretime = newPrefix.get(newPrefix.size()-1).getTimestamp();
		newPrefix.addItemset(new Itemset(item, timestamp + pretime));
		return newPrefix;
	}
	
	/**
	 * this method creates a copy of the sequence and add the item to the last itemset of the sequence. 
	 * It sets the support of the sequence as the support of the item.
	 */
	private Sequence appendItemToPrefixOfSequence(Sequence prefix, Item item) {
		Sequence newPrefix = prefix.cloneSequence();
		Itemset itemset = newPrefix.get(newPrefix.size()-1);  
		itemset.addItem(item);
		return newPrefix;
	}
	
	public void printStatistics(int objectsCount) {
			patterns.toString(objectsCount);
		}
	
	
	public double getMinSupp() {
		return minsupp;
	}

	
}
