package prefix.span;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class represents a a sequence.
 * A sequence is a list of itemsets.
  **/
public class Sequence{
	public long shift = 0;
	private int seqCount=0;
	private final List<Itemset> itemsets = new ArrayList<Itemset>();
	private int id; 
	public static String lift1=new String();
	int len=0;
	int in;
	// List of IDS of all patterns that contains this one.
	private Set<Integer> sequencesID = null;
	
	public Sequence(int id){
		this.id = id;
	}
	
	public String getRelativeSupportFormated(int transactionCount) {
		double frequence = ((double)sequencesID.size()) / ((double) transactionCount);
		DecimalFormat format = new DecimalFormat();
		format.setMinimumFractionDigits(0); 
		format.setMaximumFractionDigits(4); 
		return format.format(frequence);
	}
	
	public int getAbsoluteSupport(){
		return sequencesID.size();
	}

	public void addItemset(Itemset itemset) {
		itemsets.add(itemset);
	}
	
	public Sequence cloneSequence(){
		Sequence sequence = new Sequence(getId());
		for(Itemset itemset : itemsets){
			sequence.addItemset(itemset.cloneItemSet());
		}
		return sequence;
	}
	
	public Sequence cloneSequence1(){
		Sequence sequence = new Sequence(getId());
		int i=1;
	//	Itemset itemset=new Itemset();
//		for(int i=0;i<itemsets.size();i++){
		for(Itemset itemset : itemsets){
			
			if(i<itemsets.size()){
			sequence.addItemset(itemset.cloneItemSet());
			i++;
			}
		}
		return sequence;
	}
	

	public void print() {
		System.out.print(toString());
	}

	public String toString1() {
		StringBuffer r = new StringBuffer("");
		for(Itemset itemset : itemsets){
			seqCount=0;
			r.append("{t=");
			r.append(itemset.getTimestamp());
			r.append(", ");
			for(Item item : itemset.getItems()){
				String string = item.toString();
				r.append(string);
				r.append(' ');
			}
			r.append('}');
			
		}
		return r.toString();
	}
	
	
	public String toString() {
		StringBuffer r = new StringBuffer("");
		int i=1;
		for(Itemset itemset : itemsets){
			//seqCount=0;
			if(MainTestPrefixSpan.flag==true)
			{
			  if(i>itemsets.size()-1 && itemsets.size()>1){
				  r.append(" => ");
			  }
			}
			i++;
			r.append("{t=");
			r.append(itemset.getTimestamp());
			r.append(", ");
			for(Item item : itemset.getItems()){
				String string = item.toString();
				r.append(string);
				r.append(' ');
			}
			r.append('}');
			 len=r.length();
			 in=r.indexOf("=>");
			 if(in>=0)
			 {	 
			 lift1="{t=0,";			 
			 lift1=lift1+r.substring(in+8,len);
			 }		
			
		}
          
		return r.append("    ").toString();
	}
	
	public String itemsetsToString() {
		StringBuffer r = new StringBuffer("");
		for(Itemset itemset : itemsets){
			r.append("{t=");
			r.append(itemset.getTimestamp());
			r.append(", ");
			for(Item item : itemset.getItems()){
				String string = item.toString();
				r.append(string);
				r.append(' ');
			}
			r.append('}');
		}
		return r.append("    ").toString();
	}
	
	public int getId() {
		return id;
	}

	public List<Itemset> getItemsets() {
		return itemsets;
	}
	
	public Itemset get(int index) {
		return itemsets.get(index);
	}
	

	public Item getIthItem(int i) { 
		for(int j=0; j< itemsets.size(); j++){
			if(i < itemsets.get(j).size()){
				return itemsets.get(j).get(i);
			}
			i = i- itemsets.get(j).size();
		}
		return null;
	}
	
	public int size(){
		return itemsets.size();
	}

	public Set<Integer> getSequencesID() {
		return sequencesID;
	}

	public void setSequencesID(Set<Integer> sequencesID) {
		this.sequencesID = sequencesID;
	}
		
//removes infrequent items from current sequence	
	public Sequence cloneSequenceMinusItems(Map<Item, Set<Integer>> mapSequenceID, double relativeMinSup) {
		Sequence sequence = new Sequence(getId());
		for(Itemset itemset : itemsets){
			Itemset newItemset = itemset.cloneItemSetMinusItems(mapSequenceID, relativeMinSup);
			if(newItemset.size() !=0){ 
				sequence.addItemset(newItemset);
			}
		}
		return sequence;
	}
}
