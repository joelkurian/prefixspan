package prefix.span;

import java.util.HashSet;
import java.util.Set;


/**
 * This class represents a pair of an Item. 
 */
class Pair{
	private final long timestamp;
	private final boolean postfix;
	private final Item item;
	
	
	private Set<Integer> sequencesID = new HashSet<Integer>();
	
	Pair(long timestamp, boolean postfix, Item item){
		this.timestamp = timestamp;
		this.postfix = postfix;
		this.item = item;
	}
	
	
	Pair( boolean postfix, Item item){
		this.timestamp = 0; 
		this.postfix = postfix;
		this.item = item;
	}
	
	public boolean equals(Object object){
		Pair paire = (Pair) object;
		if((paire.timestamp == this.timestamp) && (paire.postfix == this.postfix) 
				&& (paire.item.equals(this.item))){
			return true;
		}
		return false;
	}
	
	public int hashCode()
	{
		StringBuffer r = new StringBuffer();
		r.append(timestamp);
		r.append((postfix ? 'P' : 'N'));
		r.append(item.getId());
		return r.toString().hashCode();
	}

	public long getTimestamp() {
		return timestamp;
	}

	public boolean isPostfix() {
		return postfix;
	}

	public Item getItem() {
		return item;
	}

	public int getCount() {
		return sequencesID.size();
	}		

	public Set<Integer> getSequencesID() {
		return sequencesID;
	}

	public void setSequencesID(Set<Integer> sequencesID) {
		this.sequencesID = sequencesID;
	}
}