package prefix.span;

//import SimpleTableDemo;

import java.awt.Color;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

/**
 * Implementation of a set of sequences, grouped by their size (how many items they have).
 */
public class Sequences extends JFrame {
	
	int  row=0,em,row2=0;
	 static String emdata[]=new String[100000];
	    static double emsup[]=new double[100000];
		
	double growth;
	static int row1=0;
	public final List<List<Sequence>> levels = new ArrayList<List<Sequence>>();  
	public int nbSequeencesFrequentes=0;
	public static int seqCount;
//	private final String name;
    public Sequences(String name){
    	super( name );
     	levels.add(new ArrayList<Sequence>());
     }
	
	public void toString(int nbObject){
		long recency;
		long sRecency=MainTestPrefixSpan.sRecency;
	 	String[] columnNames={"SEQUENCE",
                "SUPPORT",
                "CONFIDENCE"};
	 	//,"LIFT"};
   	    Object[][] data=new Object[100000][4];
   	    String[] columnNames1={"EMERGING PATTERN",
             "SUPPORT"};
	    Object[][] data2=new Object[100000][4];
	    
   	   int levelCount=0,i=1,d=1,t=1,l,prev=0,cur=0,q,b=1,j,copy1=0,g,w,ch=0;
		boolean curflag1=false;
		double minconf=MainTestPrefixSpan.confd;
		double conf=0.0;
		int sup[]=new int[100000];
		int levelcount1[]=new int[100000];
		String rep[]=new String[100000];
		int repsup[]=new int[100000];
		String str[]=new String[100000];
		seqCount=0;
		String liftseq[]=new String[10000];
		double liftsup[]=new double[10000];
		DecimalFormat format = new DecimalFormat();
		for(List<Sequence> level : levels){
			for(Sequence sequence : level){
				recency=sequence.get(sequence.size()-1).getTimestamp();
				levelcount1[d]=t;
				if(levelCount==1){
				str[i]=sequence.toString1();
				liftseq[ch]=sequence.toString1();
				sup[i]=sequence.getAbsoluteSupport();
				liftsup[ch]=sequence.getAbsoluteSupport()/(double)nbObject;
			//	liftsup[ch]=Double.valueOf(sequence.getRelativeSupportFormated(nbObject));
    			ch++;
			//	ch++;
				}
				if(levelCount>1){
					str[i]=sequence.cloneSequence1().toString1();
					sup[i]=sequence.getAbsoluteSupport();
				//	if(levelCount==3)
				//	System.out.println(str[i]);
					prev=levelcount1[d-1];
					for(q=1;q<=d-2;q++)
					cur=cur+levelcount1[q];
					copy1=cur+prev;
				//	System.out.println("prev :"+prev);
					for(l=cur+1;l<=prev+cur;l++){
						//System.out.println("cur :"+cur);
				//		if(levelCount==3)
					//	System.out.println(str[l]);
						if(str[i].equals(str[l])){
						//	if(levelCount==3)
						//	System.out.println("matched");
							conf=sup[i]/(double)sup[l];
							 if(conf>=minconf)
							{
													

								if(levelCount>1){
									if(curflag1==true){
							        curflag1=false;
									}
									rep[b]=sequence.cloneSequence().toString1();
									repsup[b]=sequence.getAbsoluteSupport();
									b++;
									}
 								
						        if(recency>= sRecency || MainTestPrefixSpan.recencyflag==false)
						        {
				                data[row][0]=sequence.toString();
				                seqCount++;
				                data[row][1]=sequence.getRelativeSupportFormated(nbObject);
						        }
				               if(MainTestPrefixSpan.emflag==false)
				               {
				            	   emdata[row1]=sequence.toString();
				            	  // emsup[row]=
				            	   emsup[row1]=Double.valueOf(sequence.getRelativeSupportFormated(nbObject)).doubleValue();
				            	   row1++;
				               }
				               /*  if(recency>= sRecency)
	            				seqCount++;
	            		        if(recency>= sRecency)
    							data[row][1]=sequence.getRelativeSupportFormated(nbObject);*/
    							
    						//	if(levelCount>1){
									
									format.setMinimumFractionDigits(0); 
									format.setMaximumFractionDigits(4);
									data[row][2]=format.format(conf);
									 for(w=0;w<ch;w++)
						                {
										 if(Sequence.lift1.equals(liftseq[w])){
						                	
						                	data[row][3]=getSupportFormated(conf/liftsup[w]);
						                }
										 
						                }

									 if(MainTestPrefixSpan.emflag==true)
						               {
					                	for(em=0;em<row1;em++)
					                	{
					                		if(emdata[em].equals(sequence.toString())){
					                			growth=Double.valueOf(sequence.getRelativeSupportFormated(nbObject)).doubleValue()/emsup[em];
					                		if(growth>=3)
					                		{      
					                			if(recency>= sRecency || MainTestPrefixSpan.recencyflag==false){
					                			data2[row2][0]=sequence.toString();
					                			data2[row2][1]=sequence.getRelativeSupportFormated(nbObject);
					                			row2++;
					                			}
					                		 }
					                		}
					                	}
						               }
								  if(recency>= sRecency || MainTestPrefixSpan.recencyflag==false)	 
								 	row++;
				
							}	
						}
					}
				}
				i++;
				if(levelCount==1){
					//System.out.println(curflag1);
				if(curflag1==true){	
			    curflag1=false;
				}
				
				if(recency>=sRecency || MainTestPrefixSpan.recencyflag==false){
		        data[row][0]=sequence.toString();
		        seqCount++;
    			data[row][1]=sequence.getRelativeSupportFormated(nbObject);
				}
                if(MainTestPrefixSpan.emflag==false)
	               {
	            	   emdata[row1]=sequence.toString();
		            	  // emsup[row]=
		            	   emsup[row1]=Double.valueOf(sequence.getRelativeSupportFormated(nbObject)).doubleValue();
		            	   row1++;
	               }
                if(MainTestPrefixSpan.emflag==true)
	               {
                	
                	for(em=0;em<row1;em++)
                	{
               		//	System.out.println(emdata[em]);
                		if(emdata[em].equals(sequence.toString())){
                			growth=Double.valueOf(sequence.getRelativeSupportFormated(nbObject)).doubleValue()/emsup[em];
             
                		if(growth>=3)
                		{
                			if(recency>= sRecency || MainTestPrefixSpan.recencyflag==false){
                			data2[row2][0]=sequence.toString();
                			data2[row2][1]=sequence.getRelativeSupportFormated(nbObject);
                			row2++;
                			}
                		}
                	}
                	}
	               } 	 
             
                
    			if(recency>=2 || MainTestPrefixSpan.recencyflag==false)
    	 		row++;
				}
		   		cur=0;
				t++;
			}
			if(levelCount>1)
			{
			for(j=1;j<b;j++){
				str[copy1+1]=rep[j];
				sup[copy1+1]=repsup[j];
				copy1++;
			}
          }
			b=1;
			d++;
			t=1;
			levelCount++;
			curflag1=true;
		}
	
		if(MainTestPrefixSpan.printflag==true && MainTestPrefixSpan.emflag==false){
		 JFrame frame = new JFrame("SEQUENTIAL PATTERN TABLE");
		    JPanel panel = new JPanel();
		    DefaultTableModel model = new DefaultTableModel(data,columnNames);
		    model.setRowCount(row);
		    JTable table = new JTable(model);
		   
		    Vector data1 = model.getDataVector();
		    Collections.sort(data1, new ColumnSorter(0));
		    model.fireTableStructureChanged();
		 
		    table.setFocusable(false);
		//table.setRowcount=row;
		//System.out.println(table.getRowCount());
	    JTableHeader header = table.getTableHeader();
	    header.setBackground(Color.yellow);
	    JScrollPane pane = new JScrollPane(table);
	    
	   /// table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	    //panel.add(pane);
	   // panel.setSize(700,400);
	    frame.add(pane);
	    frame.setSize(700,500);
	    frame.setUndecorated(true);
	    frame.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
	   // frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setVisible(true);
		}
		
		if( MainTestPrefixSpan.emflag==true){
			 JFrame frame = new JFrame("EMERGING PATTERN VIEW");
			    JPanel panel = new JPanel();
			    DefaultTableModel model = new DefaultTableModel(data2,columnNames1);
			    model.setRowCount(row2);
			    JTable table = new JTable(model);
			   
			    Vector data1 = model.getDataVector();
			    Collections.sort(data1, new ColumnSorter(0));
			    model.fireTableStructureChanged();
			 
			    table.setFocusable(false);
			//table.setRowcount=row;
			//System.out.println(table.getRowCount());
		    JTableHeader header = table.getTableHeader();
		    header.setBackground(Color.yellow);
		    JScrollPane pane = new JScrollPane(table);
		    
		   /// table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		    //panel.add(pane);
		   // panel.setSize(700,400);
		    frame.add(pane);
		    frame.setSize(700,500);
		    frame.setUndecorated(true);
		    frame.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
		   // frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    frame.setVisible(true);
			}
		
		
		
		
	}
	
	public void addSequence(Sequence sequence, int k){
		//int t=0;
		while(levels.size() <= k){
			levels.add(new ArrayList<Sequence>());
		}
		levels.get(k).add(sequence);
	//	t=sequence.getSequencesID().size();
		nbSequeencesFrequentes++;
	}
	
	public List<Sequence> getLevel(int index){
		return levels.get(index);
	}
	
	public int getLevelCount(){
		return levels.size();
	}

	public List<List<Sequence>> getLevels() {
		return levels;
	}
	
	public String getSupportFormated(double d) {
		double frequence = d;
		DecimalFormat format = new DecimalFormat();
		format.setMinimumFractionDigits(0); 
		format.setMaximumFractionDigits(4); 
		return format.format(d);
	}	
	 
	
}

class ColumnSorter implements Comparator {
	  int colIndex;

	  ColumnSorter(int colIndex) {
	    this.colIndex = colIndex;
	  }

	  public int compare(Object a, Object b) {
	    Vector v1 = (Vector) a;
	    Vector v2 = (Vector) b;
	    Object o1 = v1.get(colIndex);
	    Object o2 = v2.get(colIndex);

	    if (o1 instanceof String && ((String) o1).length() == 0) {
	      o1 = null;
	    }
	    if (o2 instanceof String && ((String) o2).length() == 0) {
	      o2 = null;
	    }

	    if (o1 == null && o2 == null) {
	      return 0;
	    } else if (o1 == null) {
	      return 1;
	    } else if (o2 == null) {
	      return -1;
	    } else if (o1 instanceof Comparable) {

	      return ((Comparable) o1).compareTo(o2);
	    } else {

	      return o1.toString().compareTo(o2.toString());
	    }
	  }
}




