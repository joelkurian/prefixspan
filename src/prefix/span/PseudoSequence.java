package prefix.span;


/**
 * This class represents a sequence from a projected database .
 * Since it is a projected sequence, it makes reference to the original sequence.
 * 
 */

public class PseudoSequence {

	private long timeShift; 
	private Sequence sequence;
	private int firstItemset;
	private int firstItem;
	private int lastItemset;
	private int lastItem;
	
	public PseudoSequence(long timeShift, PseudoSequence sequence, int indexItemset, int indexItem){
		this.timeShift = timeShift;
		this.sequence = sequence.sequence;
		this.firstItemset = indexItemset + sequence.firstItemset;
		if(this.firstItemset == sequence.firstItemset){
			this.firstItem = indexItem + sequence.firstItem;
		}else{
			this.firstItem = indexItem; 
		}
		this.lastItemset = sequence.lastItemset;
		this.lastItem = sequence.lastItem;
	}
	
	public  PseudoSequence(long timeShift, Sequence sequence, int indexItemset, int indexItem){
		this.timeShift = timeShift;
		this.sequence = sequence;
		this.firstItemset = indexItemset;
		this.firstItem = indexItem;
		// Last itemset and item  (by default, this is the last item & itemset of the sequence.
		this.lastItemset = sequence.size()-1;
		this.lastItem = sequence.getItemsets().get(lastItemset).size()-1;
		//System.out.println(this.lastItemset);
	}
	
	public int size(){
		int size = sequence.size() - firstItemset - ((sequence.size()-1) - lastItemset);
		if(size == 1 && sequence.getItemsets().get(firstItemset).size() == 0){
			return 0;
		}
		return size;
	}
	
	public int getSizeOfItemsetAt(int index){
		int size = sequence.getItemsets().get(index + firstItemset).size();
		if(isLastItemset(index)){
			size -= ((size -1) - lastItem);
		}
		if(isFirstItemset(index)){
			size -=  firstItem;
		}
		return size;
	}
	
	//	return true if this itemset is cut at its left.
	public boolean isPostfix(int indexItemset){
		return indexItemset == 0  && firstItem !=0;
	}
	
	public boolean isFirstItemset(int index){
		return index == 0;
	}
	
	public boolean isLastItemset(int index){
		return (index + firstItemset) == lastItemset;
	}
	
	public Item getItemAtInItemsetAt(int indexItem, int indexItemset){
		if(isFirstItemset(indexItemset)){
			return getItemset(indexItemset).get(indexItem + firstItem);
		}else{
			return getItemset(indexItemset).get(indexItem);
		}
	}
	
	public long getTimeStamp(int indexItemset){
		return getItemset(indexItemset).getTimestamp() - timeShift;
	}
	
	public long getAbsoluteTimeStamp(int indexItemset){
		return getItemset(indexItemset).getTimestamp();
	}
	
	private Itemset getItemset(int index){
		return sequence.get(index+firstItemset);
	}

	public int getId() {
		return sequence.getId();
	}

	public void print() {
		System.out.print(toString());
	}
	
	public String toString(){
		StringBuffer r = new StringBuffer();
		for(int i=0; i < size(); i++){
			r.append("{t=");
			r.append(getTimeStamp(i));
			r.append(", ");
			for(int j=0; j < getSizeOfItemsetAt(i); j++){
				if(!isLastItemset(i) || (j <= lastItem)){
					r.append(getItemAtInItemsetAt(j, i).toString());
					if(isPostfix(i)){
						r.append('*');
					}
					r.append(' ');
				}
			}
			r.append("}");
			//r.append("\n");
		}
		r.append("  ");
		return r.toString();
	}

	public int indexOf(int indexItemset, int idItem) {
		for(int i=0; i < getSizeOfItemsetAt(indexItemset); i++){
			if(getItemAtInItemsetAt(i, indexItemset).getId() == idItem){
				return i;
			}
		}
		return -1;
	}
	
		public long getTimeShift() {
		return timeShift;
	}
}