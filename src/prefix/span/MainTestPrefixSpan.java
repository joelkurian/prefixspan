package prefix.span;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * Test program
 */
public class MainTestPrefixSpan extends Panel implements ActionListener {
	String str = new String();

	JTextField t1 = new JTextField(30);

	JTextField t2 = new JTextField(10);

	JTextField t3 = new JTextField(10);

	JTextField t4 = new JTextField(10);

	JTextField t5 = new JTextField(10);

	JTextField t6 = new JTextField(10);

	JTextField t7 = new JTextField(10);

	JTextField t9 = new JTextField(10);

	JButton b1 = new JButton("View Sequential Patterns");

	JFrame result = new JFrame("Sequential Patterns");

	JFileChooser fc = new JFileChooser();

	JFrame frame = new JFrame("Message");

	JButton b2 = new JButton("Browse");

	JTextField t8 = new JTextField(30);

	JButton b3 = new JButton("View Emerging Patterns");

	JButton b4 = new JButton("Browse");

	JButton b5 = new JButton("Add Recency Constraint");

	JLabel l1 = new JLabel("Recency Minimum Support", JLabel.LEFT);

	JTextArea ta = new JTextArea(200, 200);

	JScrollPane areaScrollPane = new JScrollPane(ta);
	// JPanel p2 = new JPanel();
	// SequenceDatabase sequenceDatabase = new SequenceDatabase();
	// AlgoPrefixSpan algo;
	public static boolean printflag;
	public static long sRecency;
	public static double confd;
	public long endTime;
	public static boolean flag;
	public static boolean recencyflag = false;
	public static boolean emflag = false;

	public static void main(String[] args) {
		Frame f = new Frame();
		f.addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				System.exit(0);
			};
		});

		MainTestPrefixSpan mtps = new MainTestPrefixSpan();
		mtps.setSize(100, 100); // same size as defined in the HTML APPLET
		f.add(mtps);
		f.pack();
		mtps.init();
		f.setSize(100, 100 + 20); // add 20, seems enough for the Frame title,
		f.setVisible(true);
	}

	public void init() {
		// this.setLocation(100,100);
		this.setBackground(Color.PINK);

		b1.setVerticalTextPosition(AbstractButton.CENTER);
		b1.setHorizontalTextPosition(AbstractButton.LEADING);
		b1.setMnemonic(KeyEvent.VK_D);
		b1.setActionCommand("seq");

		b2.setVerticalTextPosition(AbstractButton.CENTER);
		b2.setHorizontalTextPosition(AbstractButton.LEADING);
		b2.setMnemonic(KeyEvent.VK_D);
		b2.setActionCommand("browse");

		b3.setVerticalTextPosition(AbstractButton.CENTER);
		b3.setHorizontalTextPosition(AbstractButton.LEADING);
		b3.setMnemonic(KeyEvent.VK_D);
		b3.setActionCommand("emerge");

		b4.setVerticalTextPosition(AbstractButton.CENTER);
		b4.setHorizontalTextPosition(AbstractButton.LEADING);
		b4.setMnemonic(KeyEvent.VK_D);
		b4.setActionCommand("browse1");

		b5.setVerticalTextPosition(AbstractButton.CENTER);
		b5.setHorizontalTextPosition(AbstractButton.LEADING);
		// b5.setMnemonic(KeyEvent.VK_D);
		b5.setActionCommand("recency");

		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JPanel p3 = new JPanel();
		// JPanel p4 = new JPanel();
		p1.setBackground(Color.PINK);
		p2.setBackground(Color.PINK);
		p3.setBackground(Color.PINK);
		// p4.setBackground(Color.PINK);
		this.setSize(550, 470);
		setLayout(new BorderLayout());
		ta.setEditable(false);
		GridLayout g = new GridLayout(8, 3);
		Canvas c = new Canvas();
		Canvas c1 = new Canvas();
		Canvas c2 = new Canvas();
		Canvas c3 = new Canvas();
		Canvas c4 = new Canvas();
		Canvas c5 = new Canvas();

		c.setSize(0, 0);
		c1.setSize(0, 0);
		c2.setSize(0, 0);
		c3.setSize(0, 0);
		c4.setSize(0, 0);
		c5.setSize(0, 0);
		p1.setLayout(g);
		g.setHgap(10);
		g.setVgap(20);
		p1.add(new JLabel("Sequence Database File (*.txt)", JLabel.LEFT));
		t1.setText("sequence database.txt");
		// t1.se.selectAll();
		p1.add(t1);
		p1.add(b2);
		p1.add(new JLabel("Minimum Support", JLabel.LEFT));
		p1.add(t2);
		t2.setText("0.001");
		p1.add(c);
		p1.add(new JLabel("Minimum Confidence", JLabel.LEFT));
		p1.add(t5);
		t5.setText("0.5");
		p1.add(c2);
		p1.add(new JLabel("Minimum Gap", JLabel.LEFT));
		p1.add(t3);
		t3.setText("1");
		p1.add(c1);
		p1.add(new JLabel("Maximum Gap", JLabel.LEFT));
		p1.add(t4);
		t4.setText("3");
		p1.add(c3);
		p1.add(new JLabel("Minimum Compactness", JLabel.LEFT));
		p1.add(t6);
		t6.setText("1");
		p1.add(c4);
		p1.add(new JLabel("Maximum Compactness", JLabel.LEFT));
		p1.add(t7);
		t7.setText("5");
		p1.add(c5);
		p1.add(new JLabel("Sequence Database File (*.txt)", JLabel.LEFT));
		p1.add(t8);
		t8.setText("emerge.txt");
		p1.add(b4);
		p2.add(new JLabel(
				"(Select New Updated Sequence Database File For Emerging Patterns)",
				JLabel.LEFT));
		// p2.add(new JLabel("                          ",JLabel.LEFT));

		// l1.setFocusable(false)
		p2.add(new JLabel(
				"                                                                 ",
				JLabel.RIGHT));
		p2.add(new JLabel(
				"                                                                 ",
				JLabel.RIGHT));
		// t9.setFocusable(false);
		// t9.setBackground(Color.LIGHT_GRAY);
		p2.add(l1);
		p2.add(t9);

		l1.setVisible(false);
		t9.setVisible(false);

		p3.add(b5);
		p3.add(b1);
		p3.add(b3);

		add(p1, BorderLayout.NORTH);
		add(p2, BorderLayout.CENTER);
		add(p3, BorderLayout.SOUTH);
		areaScrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		areaScrollPane
				.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		areaScrollPane.setPreferredSize(new Dimension(200, 200));
		result.getContentPane().add(areaScrollPane, BorderLayout.CENTER);

		result.addWindowListener(new a(this));
		b1.addActionListener(this);
		b2.addActionListener(this);
		b3.addActionListener(this);
		b4.addActionListener(this);
		b5.addActionListener(this);

	}

	public void actionPerformed(ActionEvent e) {
		SequenceDatabase sequenceDatabase = new SequenceDatabase();
		AlgoPrefixSpan algo;
		if ("seq".equals(e.getActionCommand())) {
			String s1 = t1.getText();
			if (recencyflag == true)
				sRecency = Long.parseLong((t9.getText()));
			try {
				sequenceDatabase.loadFile(fileToPath(s1));
			} catch (IOException e1) {
				System.out.println(e1.getMessage());
			}
			confd = Double.valueOf(t5.getText());

			algo = new AlgoPrefixSpan(Float.valueOf(t2.getText()).floatValue(),
					Float.valueOf(t3.getText()).floatValue(), Float.valueOf(
							t4.getText()).floatValue(), Float.valueOf(
							t6.getText()).floatValue(), Float.valueOf(
							t7.getText()).floatValue());
			flag = false;
			algo.runAlgorithm(sequenceDatabase);
			endTime = System.currentTimeMillis() - algo.startTime;
			printflag = true;
			flag = true;
			algo.printStatistics(sequenceDatabase.size());
			JOptionPane.showMessageDialog(frame, "Successful Execution \n"
					+ "Total Search Time:" + endTime / 1000 + " Seconds\n"
					+ "Number of Sequential Patterns Found:"
					+ Sequences.seqCount
					+ "\n(See Sequential Patterns for more details)\n",
					"Message", JOptionPane.INFORMATION_MESSAGE);
			if (recencyflag == true) {
				recencyflag = false;
				l1.setVisible(false);
				t9.setVisible(false);
				// t9.setFocusable(false);
				// t9.setBackground(Color.LIGHT_GRAY);
				t9.setText("");
				sRecency = 1;
			}
		}
		if ("browse".equals(e.getActionCommand())) {
			File f = new File(
					"C:/JBuilder2007/runtime-New_configuration/constrainedprefixspan/SEQUENTIAL_PATTERN_MINING/tests");
			fc.setCurrentDirectory(f);
			fc.showOpenDialog(this);
			String a_file = fc.getSelectedFile().getName();

			t1.setText(a_file);
		}

		if ("browse1".equals(e.getActionCommand())) {
			File f = new File(
					"C:/JBuilder2007/runtime-New_configuration/constrainedprefixspan/SEQUENTIAL_PATTERN_MINING/tests");
			fc.setCurrentDirectory(f);
			fc.showOpenDialog(this);
			String a_file = fc.getSelectedFile().getName();

			t8.setText(a_file);
		}

		if ("recency".equals(e.getActionCommand())) {
			recencyflag = true;
			// p2.add(l1);
			// p2.add(t9);
			l1.setVisible(true);
			t9.setVisible(true);
			t9.setFocusable(true);
			t9.setBackground(Color.WHITE);

			// t9.setFocusable(true);
			t9.setText("2");
			this.resize(new Dimension(551, 470));
			// this.setBounds(0, 0, 550, 500);
		}

		if ("emerge".equals(e.getActionCommand())) {
			if (recencyflag == true)
				sRecency = Long.parseLong((t9.getText()));

			emflag = true;
			SequenceDatabase sequenceDatabase1 = new SequenceDatabase();
			AlgoPrefixSpan algo1;
			String s2 = t8.getText();
			try {
				sequenceDatabase1.loadFile(fileToPath(s2));
			} catch (IOException e2) {
				System.out.println(e2.getMessage());
			}

			algo1 = new AlgoPrefixSpan(
					Float.valueOf(t2.getText()).floatValue(), Float.valueOf(
							t3.getText()).floatValue(), Float.valueOf(
							t4.getText()).floatValue(), Float.valueOf(
							t6.getText()).floatValue(), Float.valueOf(
							t7.getText()).floatValue());
			flag = false;
			algo1.runAlgorithm(sequenceDatabase1);
			endTime = System.currentTimeMillis() - algo1.startTime;
			printflag = true;
			flag = true;
			algo1.printStatistics(sequenceDatabase1.size());
			emflag = false;

			if (recencyflag == true) {
				recencyflag = false;
				l1.setVisible(false);
				t9.setVisible(false);
				// t9.setFocusable(false);
				// t9.setBackground(Color.LIGHT_GRAY);
				t9.setText("");
				sRecency = 1;
			}
		}
	}

	public String fileToPath(String filename)
			throws UnsupportedEncodingException {
		URL url = MainTestPrefixSpan.class.getResource(filename);
		return java.net.URLDecoder.decode(url.getPath(), "UTF-8");
	}
}

class a extends WindowAdapter {
	MainTestPrefixSpan ob;

	public a(MainTestPrefixSpan ob) {
		this.ob = ob;
	}

	public void windowClosing(WindowEvent w) {
		Frame f = (Frame) w.getSource();
		f.dispose();
		// System.exit(0);
	}

}
